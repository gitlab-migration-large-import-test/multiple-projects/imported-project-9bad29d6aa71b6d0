# frozen_string_literal: true

module Mr
  # Recreate merge request
  #
  class RecreateJob < ApplicationJob
    queue_as :high

    sidekiq_options retry: false

    # Perform merge request recreation
    #
    # @param [String] project_name
    # @param [Number] mr_iid
    # @param [String] discussion_id
    # @return [void]
    def perform(project_name, mr_iid, discussion_id = nil)
      run_within_context({ job: "mr-update", project: project_name, mr: "!#{mr_iid}" }) do
        Update::Routers::MergeRequest::Recreate.call(
          project_name: project_name,
          mr_iid: mr_iid,
          discussion_id: discussion_id
        )
      end
    end
  end
end
