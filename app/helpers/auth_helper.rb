# frozen_string_literal: true

module AuthHelper
  class AuthError < StandardError; end

  class << self
    # Authenticate user using basic authorization
    #
    # @param [String] authorization
    # @return [User]
    # @raise [AuthError]
    def authenticate_basic_auth_base64!(authorization)
      username, password = authorization
                           .split(" ").last
                           .then { |encoded| Base64.decode64(encoded).split(":") }
      authenticate_basic_auth!(username, password)
    end

    def authenticate_basic_auth!(username, password)
      user = User.find_by(username: username)
      raise AuthError, "Unauthorized" unless user.authenticate(password)

      user
    rescue Mongoid::Errors::DocumentNotFound
      raise AuthError, "Unauthorized"
    end

    # Rack app with authentication
    #
    # @return [Rack::Builder]
    def with_auth(app)
      Rack::Builder.new do
        use Auth
        run app
      end
    end
  end
end
