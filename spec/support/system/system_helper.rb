# frozen_string_literal: true

require "mustache"

require_relative "../mocks/smocker"
require_relative "../api_helper"

RSpec.shared_context "with system helper" do
  include_context "with api helper"

  let(:project_name) { "project-name" }
  let(:mock) { Support::Smocker.new }
  let(:mock_definitions) { [] }

  before do
    mock.reset
    mock.add(definition_yml)
  end

  def expect_all_mocks_called
    resp = mock.verify
    verified = resp.dig(:mocks, :verified)
    unused = resp.dig(:mocks, :unused)
    failures = resp.dig(:history, :failures)

    aggregate_failures do
      expect(verified).to eq(true), "Expected all mocks to be verified"
      expect(unused).to eq(nil), "Expected no unused mocks: #{JSON.pretty_generate(unused)}"
      expect(failures).to eq(nil), "Expected to not have failures: #{JSON.pretty_generate(failures)}"
    end
  end

  def definition_yml # rubocop:disable Metrics/CyclomaticComplexity, Metrics/PerceivedComplexity, Metrics/MethodLength
    yml_definitions = mock_definitions.map do |definition|
      if definition.is_a?(Symbol)
        name = definition
        type = :gitlab
        params = {}
      elsif definition.is_a?(Hash)
        name = definition[:name] || raise("Missing mock name")
        type = definition[:type] || :gitlab
        params = definition[:params] || {}
      else
        raise("Invalid mock definition: #{definition}")
      end

      dynamic_params = params.merge({ project_name: project_name })
      dynamic_params[:id] = Faker::Number.number(digits: 10) unless dynamic_params[:id]
      dynamic_params[:iid] = Faker::Number.number(digits: 5) unless dynamic_params[:iid]
      if name == :dep_file
        dynamic_params[:gemfile] = Base64.strict_encode64(File.read("spec/fixture/gemfiles/Gemfile"))
        dynamic_params[:gemfile_lock] = Base64.strict_encode64(File.read("spec/fixture/gemfiles/Gemfile.lock"))
      end

      Mustache.render(File.read("spec/fixture/#{type}/mocks/dynamic/#{name}.yml"), **dynamic_params)
    end

    yml_definitions.join("\n")
  end
end
