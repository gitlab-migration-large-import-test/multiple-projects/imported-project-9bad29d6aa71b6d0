# frozen_string_literal: true

require_relative "../authenticated_endpoint"
require_relative "../paginated_endpoint"

api_prefix = "/api/v2/projects"

describe V2::Projects, :aggregate_failures, :integration, type: :request do
  include_context "with api helper"

  describe api_prefix do
    let(:path) { api_prefix }
    let(:project) { create(:project) }

    context "basic auth integraton" do
      before do
        api_get(path)
      end

      it_behaves_like "basic auth endpoint"
    end

    context "get" do
      before do
        allow(Project).to receive(:all).and_return(Project.where(id: project.id))
      end

      it "lists registered projects" do
        api_get(path)

        expect_status(200)
        expect_entity_response(Project::Entity, [project])
      end

      context "with pagination" do
        before do
          allow(Project).to receive(:all).and_return(Project.where(id: project.id).or(id: create(:project).id))
          api_get(path)
        end

        it_behaves_like "paginated endpoint", { total: 2, total_pages: 1 }
      end
    end

    context "create" do
      let(:project_access_token) { "test-token" }
      let(:creator_return) { project }
      let(:user) { create(:user) }

      before do
        allow(Dependabot::Projects::Creator).to receive(:call) { creator_return }
        allow(Cron::JobSync).to receive(:call).with(project)
      end

      context "with new project" do
        let(:project) { build(:project) }

        it "creates project and jobs" do
          api_post(path, { project_name: project.name, gitlab_access_token: project_access_token })

          expect_status(201)
          expect_entity_response(Project::Entity, project)
          expect(Cron::JobSync).to have_received(:call).with(project)
          expect(Dependabot::Projects::Creator).to have_received(:call).with(
            project.name,
            access_token: project_access_token
          )
        end
      end

      context "with existing project" do
        let(:project) { create(:project) }

        it "returns error" do
          api_post(path, { project_name: project.name })
          expect_status(409)
          expect_json(error: "Project already exists")
        end
      end
    end
  end

  describe "#{api_prefix}/:id" do
    let(:path) { api_prefix }
    let(:project) { create(:project) }
    let(:user) { create(:user) }

    context "get" do
      it "returns single project using project name" do
        api_get("#{path}/#{CGI.escape(project.name)}")

        expect_status(200)
        expect_entity_response(Project::Entity, project)
      end

      it "returns single project using project id" do
        api_get("#{path}/#{project.id}")

        expect_status(200)
        expect_entity_response(Project::Entity, project)
      end

      it "returns 404 when project is not found" do
        api_get("#{path}/not-found")

        expect_status(404)
        expect_json(error: 'Document not found for class Project with attributes {:name=>"not-found"}.')
      end
    end

    context "update" do
      let(:new_name) { Faker::Alphanumeric.alpha(number: 10) }
      let(:registries) do
        {
          "ghcr" => {
            "type" => "docker_registry",
            "registry" => "ghcr.io",
            "username" => "octocat",
            "password" => "secret"
          }
        }
      end

      it "updates project" do
        api_put("#{path}/#{project.id}", { name: new_name, configuration: { registries: registries } })

        project.reload

        expect_status(200)
        expect_entity_response(Project::Entity, project)
        expect(project.name).to eq(new_name)
        expect(project.configuration.registries).to eq(Registries.new(registries))
      end
    end

    context "delete" do
      it "removes registered project and jobs" do
        api_delete("#{path}/#{project.id}")

        expect_status(204)
        expect(Project.where(id: project.id).first).to be_nil
      end
    end

    context "sync" do
      before do
        allow(Dependabot::Projects::Creator).to receive(:call) { project }
        allow(Cron::JobSync).to receive(:call).with(project)
      end

      it "syncs project" do
        api_post("#{path}/#{project.id}/sync")

        expect(Dependabot::Projects::Creator).to have_received(:call).with(project.name, access_token: nil)
        expect(Cron::JobSync).to have_received(:call).with(project)
        expect_entity_response(Project::Entity, project)
      end
    end
  end
end
