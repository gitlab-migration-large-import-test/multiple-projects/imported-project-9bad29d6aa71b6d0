ARG BASE_IMAGE=ghcr.io/dependabot-gitlab/dependabot-updater-core:latest
ARG DOCKER_VERSION=24.0.6

FROM docker:${DOCKER_VERSION}-cli as docker
FROM ${BASE_IMAGE} as core

FROM core as development

ARG CODE_DIR=${HOME}/app
WORKDIR ${CODE_DIR}

ENV BUNDLE_PATH=${HOME}/.bundle \
    BUNDLE_BIN=${HOME}/.bundle/bin

# Create directory for volume containing VS Code extensions, to avoid reinstalling on image rebuilds
RUN mkdir -p "${HOME}/.vscode-server" "${HOME}/.vscode-server-insiders"

# Copy gemfile first so cache can be reused
COPY --chown=dependabot:dependabot Gemfile Gemfile.lock ${CODE_DIR}/
RUN bundle install

FROM core as production

USER root

# Install system deps
RUN set -eux; \
    apt-get update && apt-get install -y --no-install-recommends \
    autoconf \
    ca-certificates \
    curl \
    gnupg \
    lsb-release \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/*;

# Install jemmaloc
ARG JEMALLOC_VERSION=5.3.0
ARG JEMALLOC_DOWNLOAD_URL="https://github.com/jemalloc/jemalloc/releases/download/${JEMALLOC_VERSION}/jemalloc-${JEMALLOC_VERSION}.tar.bz2"
RUN set -eux; \
    mkdir -p /usr/src/jemalloc \
    && cd /usr/src/jemalloc \
    && curl --fail --location --output jemalloc.tar.bz2 ${JEMALLOC_DOWNLOAD_URL}; \
    tar -xjf jemalloc.tar.bz2 && cd jemalloc-${JEMALLOC_VERSION}; \
    ./autogen.sh --prefix=/usr \
    && make -j "$(nproc)" install \
    && make clean; \
    rm -rf ../jemalloc.tar.bz2

# Install docker and compose plugin
ARG DOCKER_VERSION
RUN set -eux; \
    mkdir -m 0755 -p /etc/apt/keyrings \
    && curl -fsSL https://download.docker.com/linux/ubuntu/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg \
    && echo "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
    $(lsb_release -cs) stable" | tee /etc/apt/sources.list.d/docker.list > /dev/null; \
    apt-get update && apt-get install -y --no-install-recommends \
    docker-ce-cli=5:${DOCKER_VERSION}* \
    && mkdir -p /usr/libexec/docker/cli-plugins \
    && apt-get autoclean \
    && rm -rf /var/lib/apt/lists/*;
COPY --from=docker /usr/local/libexec/docker/cli-plugins/docker-compose /usr/libexec/docker/cli-plugins/docker-compose

USER dependabot
WORKDIR /home/dependabot/app

ENV BUNDLE_PATH=vendor/bundle \
    BUNDLE_WITHOUT="development:test" \
    LD_PRELOAD=/usr/lib/libjemalloc.so \
    RAILS_ENV=production

# Copy gemfile first so cache can be reused
COPY --chown=dependabot:dependabot Gemfile Gemfile.lock ./
RUN set -eux; \
    bundle install \
    && gem clean \
    && bundle clean \
    && rm -rf vendor/bundle/ruby/*/cache

COPY --chown=dependabot:dependabot ./ ./

# Smoke test image
RUN SETTINGS__GITLAB_ACCESS_TOKEN=token bundle exec rails about

ARG COMMIT_SHA
ARG PROJECT_URL
ARG VERSION
ARG BASE_IMAGE

ENV APP_VERSION=$VERSION

LABEL org.opencontainers.image.authors="andrejs.cunskis@gmail.com" \
    org.opencontainers.image.source=$PROJECT_URL \
    org.opencontainers.image.revision=$COMMIT_SHA \
    org.opencontainers.image.version=$VERSION \
    org.opencontainers.image.base.name=$BASE_IMAGE

ENTRYPOINT [ "bundle", "exec" ]
